package com.agrinenko.imageboard.repository;

import com.agrinenko.imageboard.model.User;
import com.agrinenko.imageboard.model.generic.Paged;
import com.agrinenko.imageboard.model.generic.PagedRequest;
import com.agrinenko.imageboard.model.generic.Result;

import java.util.List;

public interface UserRepository {

    Paged<User> getUsersPaged(PagedRequest pagedRequest);

    User getUserById(Long userId);

    void deleteUserById(Long userId);

    User createUser(User user);

    User updateUser(User user);
}