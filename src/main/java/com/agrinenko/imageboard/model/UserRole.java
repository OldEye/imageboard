package com.agrinenko.imageboard.model;

public enum UserRole {
    USER,
    ADMINISTRATOR
}