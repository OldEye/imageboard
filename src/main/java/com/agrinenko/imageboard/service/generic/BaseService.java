package com.agrinenko.imageboard.service.generic;

import com.agrinenko.imageboard.model.generic.Paged;
import com.agrinenko.imageboard.model.generic.PagedRequest;
import com.agrinenko.imageboard.model.generic.Referenced;
import com.agrinenko.imageboard.model.generic.Result;

public abstract class BaseService<T extends Referenced> {

    public abstract Result<String> getReferenceById(Long resourceId);

    public abstract Result<Long> getIdByReference(String reference);

    protected Result<Paged<T>> validatePageRequest(PagedRequest request) {
        if (request == null)
            return new Result<>(Result.ResultCode.WRONG_PARAM, "Page request required");
        if (request.getPageNumber() < 0)
            return new Result<>(Result.ResultCode.WRONG_PARAM, "Page number can't be lower then 0");
        if (request.getPageSize() < 1)
            return new Result<>(Result.ResultCode.WRONG_PARAM, "Page size can't be lower then 1");
        return null;
    }
}