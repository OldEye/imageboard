package com.agrinenko.imageboard.service;

import com.agrinenko.imageboard.model.Post;
import com.agrinenko.imageboard.model.User;
import com.agrinenko.imageboard.model.generic.Result;
import com.agrinenko.imageboard.model.inner.PostInner;
import com.agrinenko.imageboard.repository.PostInnerRepository;
import com.agrinenko.imageboard.service.generic.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
public class PostService extends BaseService<Post> {

    private final PostInnerRepository postInnerRepository;

    private final FileService fileService;
    private final UserService userService;

    @Autowired
    public PostService(PostInnerRepository postInnerRepository, FileService fileService, UserService userService) {
        this.postInnerRepository = postInnerRepository;
        this.fileService = fileService;
        this.userService = userService;
    }

    Result<List<Post>> getAllPostsByThreadId(Long threadId) {
        var list = new ArrayList<Post>();
        var result = postInnerRepository.getPostsByThreadId(threadId);
        result.forEach(inner -> list.add(assemblePost(inner)));
        return new Result<>(list);
    }

    public Result<Post> getPostById(Long postId) {
        var postInner = postInnerRepository.getPostById(postId);
        if (postInner == null) return new Result<>(Result.ResultCode.NOT_EXISTS);
        return new Result<>(assemblePost(postInner));
    }

    public Result deletePostById(Long postId) {
        postInnerRepository.deletePostById(postId);
        return Result.ok();
    }

    Result<Post> createPost(Post post) {
        var validationResult = validatePost(post);
        if (validationResult != null) return validationResult;

        post.setPostedDate(LocalDateTime.now());
        post.setId(null);
        var inner = postInnerRepository.createPost(innerByPost(post));
        return new Result<>(assemblePost(inner));
    }

    private Post assemblePost(PostInner inner) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm:ss");

        var post = new Post();
        post.setId(inner.getId());
        post.setPostedDate(inner.getPostedDate());
        post.setPostedDateString(inner.getPostedDate().format(formatter));

        post.setText(inner.getText());
        post.setThreadId(inner.getThreadId());

        var postedBy = userService.getUserById(inner.getPostedByUserId());
        if (postedBy != null && postedBy.isOk())
            post.setPostedBy(postedBy.getContent());
        else {
            var anon = new User();
            anon.setNickname("Anonymous");
            post.setPostedBy(anon);
        }

        post.setMediaId(inner.getMediaId());

        post.setReference(getReferenceById(inner.getId()).getContent());

        return post;
    }

    private PostInner innerByPost(Post post) {
        var inner = new PostInner();
        inner.setId(post.getId());
        inner.setText(post.getText());
        inner.setPostedByUserId(post.getPostedBy().getId());
        inner.setPostedDate(post.getPostedDate());
        inner.setThreadId(post.getThreadId());
        inner.setMediaId(post.getMediaId());
        return inner;
    }

    Result<Post> validatePost(Post post) {
        if (post.getText() == null || post.getText().isEmpty())
            return new Result<>(Result.ResultCode.WRONG_PARAM, "Post must have not empty text");
        if (post.getReplyToPostRef() != null) {
            var replyToId = getIdByReference(post.getReplyToPostRef());
            if (!replyToId.isOk())
                return new Result<>(Result.ResultCode.WRONG_PARAM,
                        "Post must be not reply to any post or reply to valid existing post");
        }
        if (post.getMediaId() != null)
            if (!fileService.getFile(post.getMediaId()).isOk())
                return new Result<>(Result.ResultCode.WRONG_PARAM,
                        "Post must have reference to valid file");
        return null;
    }

    @Override
    public Result<String> getReferenceById(Long postId) {
        var post = postInnerRepository.getPostById(postId);
        if (post != null)
            return new Result<>("api/thread/" + post.getThreadId() + "/post/" + postId);
        return new Result<>(Result.ResultCode.NOT_EXISTS);
    }

    @Override
    public Result<Long> getIdByReference(String reference) {
        var idString = reference.split("/");
        try {
            var id = Long.parseLong(idString[idString.length - 1]);
            if (postInnerRepository.getPostById(id) != null)
                return new Result<>(id);
            return new Result<>(Result.ResultCode.NOT_EXISTS);
        } catch (IndexOutOfBoundsException | NumberFormatException e) {
            return new Result<>(Result.ResultCode.WRONG_PARAM, "Reference string must be reference of post");
        }
    }
}