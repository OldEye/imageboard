package com.agrinenko.imageboard.api;

import com.agrinenko.imageboard.api.generic.BaseRestController;
import com.agrinenko.imageboard.model.generic.Result;
import com.agrinenko.imageboard.service.PostService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/posts/")
public class PostsController extends BaseRestController {

    private final PostService service;

    public PostsController(PostService service) {
        this.service = service;
    }

    @GetMapping("{postId}")
    public ResponseEntity getPostById(@PathVariable Long postId) {
        Result result = service.getPostById(postId);
        return generateResponseEntity(result);
    }

    @DeleteMapping("{postId}")
    public ResponseEntity deletePostById(@PathVariable Long postId) {
        Result result = service.deletePostById(postId);
        return generateResponseEntity(result);
    }
}