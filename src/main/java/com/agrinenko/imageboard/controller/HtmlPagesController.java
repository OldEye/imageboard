package com.agrinenko.imageboard.controller;

import com.agrinenko.imageboard.model.ThreadLoadingPolicy;
import com.agrinenko.imageboard.model.UserRole;
import com.agrinenko.imageboard.service.*;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

@Controller
public class HtmlPagesController implements ErrorController {

    private final BoardService boardService;
    private final ThreadService threadService;
    private final UserService userService;

    public HtmlPagesController(BoardService boardService, ThreadService threadService, UserService userService) {
        this.boardService = boardService;
        this.threadService = threadService;
        this.userService = userService;
    }

    @GetMapping("/")
    public String getBoards() {
        return "redirect:boards.html";
    }

    @GetMapping("/index")
    public String getBoardsIndex() {
        return "redirect:boards.html";
    }

    @GetMapping("/index.html")
    public String getBoardsIndexHtml() {
        return "redirect:boards.html";
    }

    @GetMapping("/boards/board/{boardId}")
    public String getBoard(@CookieValue(value = "accessToken", required = false) String accessToken, @PathVariable(value = "boardId") Long boardId, Model model) {
        if (accessToken != null) {
            var user = userService.getUserByToken(accessToken);
            if (user.isOk() && user.getContent().getUserRole() == UserRole.ADMINISTRATOR)
                model.addAttribute("isAdmin", true);
            else model.addAttribute("isAdmin", false);
        } else model.addAttribute("isAdmin", false);

        var board = boardService.getBoardById(boardId);
        if (!board.isOk()) return "small_error";
        var threads = threadService.getAllThreadsByBoardId(boardId);
        model.addAttribute("boardId", boardId);
        model.addAttribute("name", board.getContent().getName());
        model.addAttribute("threads", threads);
        model.addAllAttributes(threads);
        return "board";
    }

    @GetMapping("/threads/{threadId}")
    public String getThread(@PathVariable(value = "threadId") Long threadId, Model model) {
        var thread = threadService.getThreadById(threadId, ThreadLoadingPolicy.ALL_POSTS);

        if (!thread.isOk()) return "small_error";

        model.addAttribute("thread", thread.getContent());
        return "thread";
    }

    @RequestMapping("/error")
    public String handleError(HttpServletRequest request) {
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);

        if (status != null) {
            int statusCode = Integer.parseInt(status.toString());
            if (statusCode == HttpStatus.NOT_FOUND.value())
                return "small_error";
        }
        return "big_error";
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}