package com.agrinenko.imageboard.model;

import com.agrinenko.imageboard.model.generic.Referenced;

import java.time.LocalDateTime;
import java.util.List;

public class Thread extends Referenced {
    private Long id;

    private Long boardId;

    private Post initialPost;
    private List<Post> answers;

    private LocalDateTime creationDate;
    private LocalDateTime lastAnswerDate;
    private String creationDateString;
    private String lastAnswerDateString;

    private Integer maxAnswersCount;
    private Integer answersCount;

    private Boolean toDestroy;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Post getInitialPost() {
        return initialPost;
    }

    public void setInitialPost(Post initialPost) {
        this.initialPost = initialPost;
    }

    public List<Post> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Post> answers) {
        this.answers = answers;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDateTime getLastAnswerDate() {
        return lastAnswerDate;
    }

    public void setLastAnswerDate(LocalDateTime lastAnswerDate) {
        this.lastAnswerDate = lastAnswerDate;
    }

    public Integer getMaxAnswersCount() {
        return maxAnswersCount;
    }

    public void setMaxAnswersCount(Integer maxAnswersCount) {
        this.maxAnswersCount = maxAnswersCount;
    }

    public Integer getAnswersCount() {
        return answersCount;
    }

    public void setAnswersCount(Integer answersCount) {
        this.answersCount = answersCount;
    }

    public Boolean getToDestroy() {
        return toDestroy;
    }

    public void setToDestroy(Boolean toDestroy) {
        this.toDestroy = toDestroy;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public String getCreationDateString() {
        return creationDateString;
    }

    public void setCreationDateString(String creationDateString) {
        this.creationDateString = creationDateString;
    }

    public String getLastAnswerDateString() {
        return lastAnswerDateString;
    }

    public void setLastAnswerDateString(String lastAnswerDateString) {
        this.lastAnswerDateString = lastAnswerDateString;
    }
}