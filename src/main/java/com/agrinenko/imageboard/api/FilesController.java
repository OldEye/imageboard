package com.agrinenko.imageboard.api;

import com.agrinenko.imageboard.api.generic.BaseRestController;
import com.agrinenko.imageboard.model.UserRole;
import com.agrinenko.imageboard.model.generic.Result;
import com.agrinenko.imageboard.model.inner.FileInner;
import com.agrinenko.imageboard.service.FileService;
import com.agrinenko.imageboard.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.URLConnection;
import java.util.UUID;

@RestController
@RequestMapping("api/files")
public class FilesController extends BaseRestController {

    private final FileService service;

    private final UserService userService;

    @Autowired
    public FilesController(FileService service, UserService userService) {
        this.service = service;
        this.userService = userService;
    }

    @GetMapping("{fileToken}")
    public ResponseEntity getFile(@PathVariable UUID fileToken) {
        var result = service.getFile(fileToken);

        if (result.isOk()) {
            var content = result.getContent();
            var resource = new ByteArrayResource(content.getFile());
            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + content.getName());
            var fileNameMap = URLConnection.getFileNameMap();
            String mimeType = fileNameMap.getContentTypeFor(content.getName());
            headers.add(HttpHeaders.CONTENT_TYPE, mimeType);

            return new ResponseEntity<>(resource, headers, HttpStatus.OK);
        } else return generateResponseEntity(result);
    }

    @PostMapping
    public ResponseEntity createFile(@RequestParam(name = "file") MultipartFile file) throws IOException {
        var fileInner = new FileInner();
        fileInner.setFile(file.getBytes());
        fileInner.setExtension(file.getContentType());
        fileInner.setName(file.getOriginalFilename());
        var result = service.createFile(fileInner);
        return generateResponseEntity(result);
    }

    @DeleteMapping("{fileToken}")
    public ResponseEntity deleteFile(@RequestHeader(name = "Authorization") String token, @PathVariable UUID fileToken) {
        var tokenResult = userService.getUserByToken(token);
        if (tokenResult.isOk()) {
            var currentUser = tokenResult.getContent();
            if (currentUser.getUserRole() != UserRole.ADMINISTRATOR) {
                var result = service.deleteFile(fileToken);
                return generateResponseEntity(result);
            }
        }
        return generateResponseEntity(new Result(Result.ResultCode.FORBIDDEN));
    }
}