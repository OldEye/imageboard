package com.agrinenko.imageboard.infrastructure.user.credentials;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserCredentialsEntityRepository extends PagingAndSortingRepository<UserCredentialsEntity, Long> {
    Optional<UserCredentialsEntity> findByLoginAndPasswordHash(String login, Integer passwordHash);

    Optional<UserCredentialsEntity> findByAccessToken(String token);
}