package com.agrinenko.imageboard.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class RecaptchaService {
    @Value("${google.recaptcha.key.secret}")
    private String captchaServerSecret;

    private static final String GOOGLE_RECAPTCHA_VERIFY_URL = "https://www.google.com/recaptcha/api/siteverify";

    @Autowired
    RestTemplateBuilder restTemplateBuilder;

    public String verifyRecaptcha(String ip, String recaptchaResponse) {
        var body = new HashMap<>();
        body.put("secret", captchaServerSecret);
        body.put("response", recaptchaResponse);
        body.put("remoteip", ip);
        ResponseEntity<Map> recaptchaResponseEntity =
                restTemplateBuilder.build()
                        .postForEntity(GOOGLE_RECAPTCHA_VERIFY_URL + "?secret={secret}&response={response}&remoteip={remoteip}", body, Map.class, body);

        var responseBody = recaptchaResponseEntity.getBody();

        boolean recaptchaSuccess = (Boolean) responseBody.get("success");
        if (!recaptchaSuccess) {
            List<String> errorCodes = (List) responseBody.get("error-codes");

            return errorCodes.stream().map(RecaptchaUtil.RECAPTCHA_ERROR_CODE::get).collect(Collectors.joining(", "));
        } else {
            return null;
        }
    }

    public static class RecaptchaUtil {

        static final Map<String, String>
                RECAPTCHA_ERROR_CODE = new HashMap<>();

        static {
            RECAPTCHA_ERROR_CODE.put("missing-input-secret",
                    "The secret parameter is missing");
            RECAPTCHA_ERROR_CODE.put("invalid-input-secret",
                    "The secret parameter is invalid or malformed");
            RECAPTCHA_ERROR_CODE.put("missing-input-response",
                    "The response parameter is missing");
            RECAPTCHA_ERROR_CODE.put("invalid-input-response",
                    "The response parameter is invalid or malformed");
            RECAPTCHA_ERROR_CODE.put("bad-request",
                    "The request is invalid or malformed");
        }
    }
}
