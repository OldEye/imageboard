package com.agrinenko.imageboard.model;

public enum ThreadLoadingPolicy {
    ALL_POSTS("all"),
    INITIAL_POST_ONLY("initial"),
    INITIAL_POST_AND_LAST("initialAndLast"),
    INITIAL_POST_AND_TOP("initialAndTop");

    private final String reduction;

    ThreadLoadingPolicy(String reduction) {
        this.reduction = reduction;
    }

    public String getReduction() {
        return reduction;
    }

    public static ThreadLoadingPolicy byReduction(String reduction) {
        if (reduction == null) return null;
        for (var lang : ThreadLoadingPolicy.values())
            if (lang.reduction.equals(reduction))
                return lang;
        return null;
    }
}