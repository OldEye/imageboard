package com.agrinenko.imageboard.infrastructure.post;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface PostEntityRepository extends PagingAndSortingRepository<PostEntity, Long> {

    Collection<PostEntity> findAllByThreadId(Long threadId);
}