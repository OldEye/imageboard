package com.agrinenko.imageboard.infrastructure.thread;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ThreadEntityRepository extends PagingAndSortingRepository<ThreadEntity, Long> {
}