package com.agrinenko.imageboard.model.inner;

public class ThreadInner {
    private Long id;
    private Long boardId;
    private Long initialPostId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getInitialPostId() {
        return initialPostId;
    }

    public void setInitialPostId(Long initialPostId) {
        this.initialPostId = initialPostId;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }
}