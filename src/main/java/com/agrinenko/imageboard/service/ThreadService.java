package com.agrinenko.imageboard.service;

import com.agrinenko.imageboard.model.Post;
import com.agrinenko.imageboard.model.Thread;
import com.agrinenko.imageboard.model.ThreadLoadingPolicy;
import com.agrinenko.imageboard.model.generic.Paged;
import com.agrinenko.imageboard.model.generic.PagedRequest;
import com.agrinenko.imageboard.model.generic.Result;
import com.agrinenko.imageboard.model.inner.ThreadInner;
import com.agrinenko.imageboard.repository.ThreadInnerRepository;
import com.agrinenko.imageboard.service.generic.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Service
public class ThreadService extends BaseService<Thread> {

    @Value("${thread_loading_policy_preview_posts_count}")
    private Integer previewPostsCount;

    private final String apiPath = "/api/threads/";

    private final ThreadInnerRepository threadInnerRepository;
    private final BoardService boardService;
    private final PostService postService;

    @Autowired
    public ThreadService(ThreadInnerRepository threadInnerRepository, BoardService boardService, PostService postService) {
        this.threadInnerRepository = threadInnerRepository;
        this.boardService = boardService;
        this.postService = postService;
    }

    public List<Thread> getAllThreadsByBoardId(Long boardId) {
        var request = new PagedRequest();
        request.setPageNumber(0);
        request.setPageSize(100000);
        var res = getThreadsPaged(request, ThreadLoadingPolicy.INITIAL_POST_AND_LAST);
        if (!res.isOk()) return new ArrayList<>();
        var list = new ArrayList<Thread>();
        res.getContent().getContent().forEach(p -> {
                    if (p.getBoardId().equals(boardId))
                        list.add(p);
                }
        );
        return list;
    }

    public Result<Paged<Thread>> getThreadsPaged(PagedRequest request, ThreadLoadingPolicy loadingPolicy) {
        var page = new ArrayList<Thread>();
        var result = threadInnerRepository.getThreadsPaged(request);
        result.getContent().forEach(inner -> page.add(assembleThread(inner, loadingPolicy)));
        return new Result<>(new Paged<>(page, result.getPage(), result.getTotalPages(), result.getRequestedPageSize(), result.getTotalElements()));
    }

    public Result<Thread> getThreadById(Long threadId, ThreadLoadingPolicy loadingPolicy) {
        var result = threadInnerRepository.getThreadById(threadId);
        if (result == null) return new Result<>(Result.ResultCode.NOT_EXISTS);
        return new Result<>(assembleThread(result, loadingPolicy));
    }

    public Result deleteThreadById(Long threadId) {
        threadInnerRepository.deleteThreadById(threadId);
        return Result.ok();
    }

    public Result<Thread> createThread(Thread thread) {
        var validationResult = validateThread(thread);
        if (validationResult != null) return validationResult;
        var initialPost = postService.createPost(thread.getInitialPost());
        thread.setInitialPost(initialPost.getContent());
        var result = threadInnerRepository.createThread(innerFromThread(thread));
        return new Result<>(assembleThread(result, ThreadLoadingPolicy.ALL_POSTS));
    }

    public Result<Post> createPost(Post post) {
        var thread = getThreadById(post.getThreadId(), ThreadLoadingPolicy.INITIAL_POST_ONLY);
        if (thread.isOk()) {
            return postService.createPost(post);
        }
        return new Result<>(Result.ResultCode.WRONG_PARAM, "Post must have valid thread param");
    }

    private Thread assembleThread(ThreadInner inner, ThreadLoadingPolicy policy) {
        var thread = new Thread();
        thread.setId(inner.getId());
        thread.setInitialPost(postService.getPostById(inner.getInitialPostId()).getContent());

        thread.setCreationDate(thread.getInitialPost().getPostedDate());
        thread.setCreationDateString(thread.getInitialPost().getPostedDateString());
        thread.setLastAnswerDate(thread.getInitialPost().getPostedDate());
        thread.setLastAnswerDateString(thread.getInitialPost().getPostedDateString());
        thread.setAnswersCount(0);

        var posts = postService.getAllPostsByThreadId(inner.getId());
        if (posts.isOk()) {
            var listNew = new ArrayList<Post>();
            var counter = 0;
            thread.setAnswersCount(posts.getContent().size());
            switch (policy) {
                case ALL_POSTS:
                    posts.getContent().sort(Comparator.comparing(Post::getId));
                    posts.getContent().removeIf(post -> post.getId().equals(thread.getInitialPost().getId()));
                    thread.setAnswers(posts.getContent());
                    break;
                case INITIAL_POST_ONLY:
                    thread.setAnswers(new ArrayList<>());
                    break;
                case INITIAL_POST_AND_TOP:
                    posts.getContent().sort(Comparator.comparing(Post::getId));
                    posts.getContent().removeIf(post -> post.getId().equals(thread.getInitialPost().getId()));
                    for (Post post : posts.getContent()) {
                        listNew.add(post);
                        counter++;
                        if (counter > previewPostsCount) break;
                    }
                    posts.setContent(listNew);
                    thread.setAnswers(posts.getContent());
                    break;
                case INITIAL_POST_AND_LAST:
                    posts.getContent().sort(Comparator.comparing(Post::getId).reversed());
                    posts.getContent().removeIf(post -> post.getId().equals(thread.getInitialPost().getId()));
                    for (Post post : posts.getContent()) {
                        listNew.add(post);
                        counter++;
                        if (counter > previewPostsCount) break;
                    }
                    posts.setContent(listNew);
                    posts.getContent().sort(Comparator.comparing(Post::getId));
                    thread.setAnswers(posts.getContent());
                    break;
            }
        }

        if (!thread.getAnswers().isEmpty()) {
            var date = thread.getAnswers()
                    .get(thread.getAnswers().size() - 1).getPostedDate();
            thread.setLastAnswerDate(date);
        }

        var board = boardService.getBoardById(inner.getBoardId());
        if (board.isOk()) {
            thread.setBoardId(board.getContent().getId());
            //thread.setToDestroy(thread.getMaxAnswersCount() < thread.getAnswersCount());
        }

        return thread;
    }

    private Result<Thread> validateThread(Thread thread) {
        if (thread.getInitialPost() == null)
            return new Result<>(Result.ResultCode.WRONG_PARAM, "Thread's initial post must be specified");

        if (postService.validatePost(thread.getInitialPost()) != null)
            return new Result<>(Result.ResultCode.WRONG_PARAM, "Thread's initial post must be valid");

        if (thread.getBoardId() == null || !boardService.getBoardById(thread.getBoardId()).isOk())
            return new Result<>(Result.ResultCode.WRONG_PARAM, "Thread's board reference must be specified and valid");
        return null;
    }

    private ThreadInner innerFromThread(Thread thread) {
        var inner = new ThreadInner();
        inner.setId(thread.getId());
        inner.setBoardId(thread.getBoardId());
        inner.setInitialPostId(thread.getInitialPost().getId());
        return inner;
    }

    @Override
    public Result<String> getReferenceById(Long resourceId) {
        return new Result<>(apiPath + resourceId);
    }

    @Override
    public Result<Long> getIdByReference(String reference) {
        var idString = reference.replace(apiPath, "");
        try {
            var id = Long.parseLong(idString);
            if (threadInnerRepository.getThreadById(id) != null)
                return new Result<>(id);
            return new Result<>(Result.ResultCode.NOT_EXISTS);
        } catch (NumberFormatException e) {
            return new Result<>(Result.ResultCode.WRONG_PARAM, "Reference string must be reference of thread");
        }
    }
}