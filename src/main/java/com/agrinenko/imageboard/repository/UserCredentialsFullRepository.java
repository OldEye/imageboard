package com.agrinenko.imageboard.repository;

import com.agrinenko.imageboard.model.UserCredentials;
import com.agrinenko.imageboard.model.inner.UserCredentialsFull;
import org.springframework.stereotype.Repository;

@Repository
public interface UserCredentialsFullRepository {

    UserCredentialsFull getUserCredentialsById(Long userCredentialsId);

    void deleteUserCredentialsById(Long userCredentialsId);

    UserCredentialsFull createUserCredentials(UserCredentials userCredentials);

    UserCredentialsFull updateUserCredentials(UserCredentialsFull userCredentials);

    Boolean isUserWithLoginExists(UserCredentials userCredentials);

    UserCredentialsFull getUserCredentialsFullByCredentials(UserCredentials userCredentials);

    UserCredentialsFull getUserCredentialsFullByToken(String token);

    void setAccessTokenToUser(Long userId, String token);
}