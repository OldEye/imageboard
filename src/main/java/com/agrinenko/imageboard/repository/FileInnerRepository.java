package com.agrinenko.imageboard.repository;

import com.agrinenko.imageboard.model.inner.FileInner;

import java.util.UUID;

public interface FileInnerRepository {

    UUID saveFile(FileInner file);

    void deleteFile(UUID token);

    FileInner getFile(UUID token);

    String getFileName(UUID token);
}