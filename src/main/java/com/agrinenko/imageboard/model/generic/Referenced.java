package com.agrinenko.imageboard.model.generic;

public abstract class Referenced {
    private String reference;

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }
}