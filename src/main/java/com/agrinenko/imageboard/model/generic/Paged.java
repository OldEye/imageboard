package com.agrinenko.imageboard.model.generic;

import java.util.List;

public class Paged<T> {

    private List<T> content;

    private Integer page;
    private Integer totalPages;

    private Integer pageSize;
    private Integer requestedPageSize;

    private Long totalElements;

    public Paged(List<T> content, Integer page, Integer totalPages, Integer requestedPageSize, Long totalElements) {
        this.content = content;
        this.page = page;
        this.totalPages = totalPages;
        this.requestedPageSize = requestedPageSize;
        this.totalElements = totalElements;
        setPageSize();
    }

    public Paged(List<T> content, Integer page, Integer totalPages) {
        this.content = content;
        this.page = page;
        this.totalPages = totalPages;
        setPageSize();
    }

    public Paged(List<T> content, Integer page) {
        this.content = content;
        this.page = page;
        setPageSize();
    }

    public Paged(List<T> content) {
        this.content = content;
        setPageSize();
    }

    public List<T> getContent() {
        return content;
    }

    public void setContent(List<T> content) {
        this.content = content;
        setPageSize();
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public Integer getRequestedPageSize() {
        return requestedPageSize;
    }

    public void setRequestedPageSize(Integer requestedPageSize) {
        this.requestedPageSize = requestedPageSize;
    }

    private void setPageSize() {
        if (content != null)
            pageSize = content.size();
        else
            pageSize = 0;
    }

    public Long getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(Long totalElements) {
        this.totalElements = totalElements;
    }
}