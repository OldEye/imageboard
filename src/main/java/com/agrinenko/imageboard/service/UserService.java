package com.agrinenko.imageboard.service;

import com.agrinenko.imageboard.model.User;
import com.agrinenko.imageboard.model.UserCredentials;
import com.agrinenko.imageboard.model.UserRole;
import com.agrinenko.imageboard.model.generic.Paged;
import com.agrinenko.imageboard.model.generic.PagedRequest;
import com.agrinenko.imageboard.model.generic.Result;
import com.agrinenko.imageboard.repository.UserCredentialsFullRepository;
import com.agrinenko.imageboard.repository.UserRepository;
import com.agrinenko.imageboard.service.generic.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.util.Base64;
import java.util.Comparator;

@Service
public class UserService extends BaseService<User> {

    @Value("${authorization.token_lifetime_seconds}")
    private Integer tokenLifetimeSeconds;

    private final String apiPath = "/api/users/";

    private final UserRepository userRepository;
    private final UserCredentialsFullRepository userCredentialsFullRepository;

    @Autowired
    public UserService(UserRepository userRepository, UserCredentialsFullRepository userCredentialsFullRepository) {
        this.userRepository = userRepository;
        this.userCredentialsFullRepository = userCredentialsFullRepository;
    }

    public Result<Paged<User>> getUsersPaged(PagedRequest request) {
        var requestValidationResult = validatePageRequest(request);
        if (requestValidationResult != null) return requestValidationResult;
        var users = userRepository.getUsersPaged(request);
        users.getContent().forEach(user -> user.setReference(getReferenceStringById(user.getId())));
        users.getContent().sort(Comparator.comparing(User::getId));
        return new Result<>(users);
    }

    public Result<User> getUserById(Long userId) {
        if (userId == null) return new Result<>(Result.ResultCode.NOT_EXISTS);
        var user = userRepository.getUserById(userId);
        if (user == null) return new Result<>(Result.ResultCode.NOT_EXISTS);
        user.setReference(getReferenceStringById(user.getId()));
        return new Result<>(user);
    }

    public Result deleteUserById(Long userId) {
        if (userId == null) return Result.notExists();
        var user = userRepository.getUserById(userId);
        if (user == null)
            return new Result(Result.ResultCode.OK);
        userCredentialsFullRepository.deleteUserCredentialsById(userId);
        userRepository.deleteUserById(userId);
        return new Result(Result.ResultCode.OK);
    }

    public Result<String> createUser(UserCredentials credentials) {
        var validationResult = validateUserCredentials(credentials);
        if (validationResult != null) return validationResult;

        if (userCredentialsFullRepository.isUserWithLoginExists(credentials))
            return new Result<>(Result.ResultCode.WRONG_PARAM, "User with this login already exists");
        var credentialsNew = userCredentialsFullRepository.createUserCredentials(credentials);
        var newUser = new User();
        newUser.setId(credentialsNew.getId());
        newUser.setNickname(credentials.getLogin());
        newUser.setAbout("");
        newUser.setUserRole(UserRole.USER);
        userRepository.createUser(newUser);

        return new Result<>(generateAccessToken(newUser.getId()));
    }

    public Result<User> updateUser(User user) {
        var validationResult = validateUser(user);
        if (validationResult != null) return validationResult;
        if (user.getId() == null) return new Result<>(Result.ResultCode.NOT_EXISTS);

        var userNew = userRepository.updateUser(user);
        userNew.setReference(getReferenceStringById(userNew.getId()));
        return new Result<>(userNew);
    }

    public Result updateUserCredentials(Long userId, UserCredentials userCredentials) {
        var validationResult = validateUserCredentials(userCredentials);
        if (validationResult != null) return validationResult;

        var credentialsFull = userCredentialsFullRepository.getUserCredentialsById(userId);
        if (credentialsFull == null) return Result.notExists();

        credentialsFull.setLogin(userCredentials.getLogin());
        credentialsFull.setPassword(userCredentials.getPassword());

        userCredentialsFullRepository.updateUserCredentials(credentialsFull);
        return Result.ok();
    }

    public Result<String> getTokenByCredentials(UserCredentials userCredentials) {
        var validationResult = validateUserCredentials(userCredentials);
        if (validationResult != null) return validationResult;

        var credentialsFull = userCredentialsFullRepository.getUserCredentialsFullByCredentials(userCredentials);
        if (credentialsFull == null) return new Result<>(Result.ResultCode.NOT_EXISTS, "User with this login and password don't exists");
        return new Result<>(generateAccessToken(credentialsFull.getId()));
    }

    public Result<User> getUserByToken(String token) {
        if (token == null || token.isEmpty())
            return new Result<>(Result.ResultCode.WRONG_PARAM, "Token can't be empty");
        var credentials = userCredentialsFullRepository.getUserCredentialsFullByToken(token);
        if (credentials == null) return new Result<>(Result.ResultCode.NOT_EXISTS);
        if (LocalDateTime.now().minusSeconds(tokenLifetimeSeconds).isAfter(credentials.getAccessTokenCreationDate())) {
            generateAccessToken(credentials.getId());
            return new Result<>(Result.ResultCode.FORBIDDEN);
        }
        var result = userRepository.getUserById(credentials.getId());
        result.setReference(getReferenceStringById(result.getId()));
        return new Result<>(result);
    }

    private Result<User> validateUser(User user) {
        if (user == null)
            return new Result<>(Result.ResultCode.WRONG_PARAM, "User can't be empty");
        if (user.getNickname() == null || user.getNickname().isEmpty())
            return new Result<>(Result.ResultCode.WRONG_PARAM, "User nickname can't be empty");
        if (user.getUserRole() == null)
            return new Result<>(Result.ResultCode.WRONG_PARAM, "User role can't be empty");
        return null;
    }

    private Result<String> validateUserCredentials(UserCredentials userCredentials) {
        if (userCredentials == null)
            return new Result<>(Result.ResultCode.WRONG_PARAM, "User credentials can't be empty");
        if (userCredentials.getLogin() == null || userCredentials.getLogin().isEmpty())
            return new Result<>(Result.ResultCode.WRONG_PARAM, "Credentials login can't be empty");
        if (userCredentials.getPassword() == null || userCredentials.getPassword().isEmpty())
            return new Result<>(Result.ResultCode.WRONG_PARAM, "Credentials password can't be empty");
        return null;
    }

    private String getReferenceStringById(Long userId) {
        return apiPath + userId.toString();
    }

    @Override
    public Result<String> getReferenceById(Long userId) {
        if (userCredentialsFullRepository.getUserCredentialsById(userId) != null)
            return new Result<>(getReferenceStringById(userId));
        return new Result<>(Result.ResultCode.NOT_EXISTS);
    }

    @Override
    public Result<Long> getIdByReference(String reference) {
        var idString = reference.replace(apiPath, "");
        try {
            var id = Long.parseLong(idString);
            if (userCredentialsFullRepository.getUserCredentialsById(id) != null)
                return new Result<>(id);
            return new Result<>(Result.ResultCode.NOT_EXISTS);
        } catch (NumberFormatException e) {
            return new Result<>(Result.ResultCode.WRONG_PARAM, "Reference string must be reference of user");
        }
    }

    public String generateAccessToken(Long userId) {
        SecureRandom secureRandom = new SecureRandom();
        Base64.Encoder base64Encoder = Base64.getUrlEncoder();
        byte[] randomBytes = new byte[24];
        secureRandom.nextBytes(randomBytes);
        var token = base64Encoder.encodeToString(randomBytes);
        userCredentialsFullRepository.setAccessTokenToUser(userId, token);
        return token;
    }
}