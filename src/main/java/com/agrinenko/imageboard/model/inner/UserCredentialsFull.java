package com.agrinenko.imageboard.model.inner;

import com.agrinenko.imageboard.model.UserCredentials;

import java.time.LocalDateTime;

public class UserCredentialsFull extends UserCredentials {
    private Long id;
    private String accessToken;
    private LocalDateTime accessTokenCreationDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public LocalDateTime getAccessTokenCreationDate() {
        return accessTokenCreationDate;
    }

    public void setAccessTokenCreationDate(LocalDateTime accessTokenCreationDate) {
        this.accessTokenCreationDate = accessTokenCreationDate;
    }
}