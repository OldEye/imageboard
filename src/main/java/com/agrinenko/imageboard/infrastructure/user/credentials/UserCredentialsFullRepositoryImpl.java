package com.agrinenko.imageboard.infrastructure.user.credentials;

import com.agrinenko.imageboard.model.UserCredentials;
import com.agrinenko.imageboard.model.inner.UserCredentialsFull;
import com.agrinenko.imageboard.repository.UserCredentialsFullRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository
public class UserCredentialsFullRepositoryImpl implements UserCredentialsFullRepository {

    private final UserCredentialsEntityRepository userCredentialsEntityRepository;

    @Autowired
    public UserCredentialsFullRepositoryImpl(UserCredentialsEntityRepository userCredentialsEntityRepository) {
        this.userCredentialsEntityRepository = userCredentialsEntityRepository;
    }

    @Override
    public UserCredentialsFull getUserCredentialsById(Long userCredentialsId) {
        var result = userCredentialsEntityRepository.findById(userCredentialsId);
        return result.map(this::credentialsFromEntity).orElse(null);
    }

    @Override
    public void deleteUserCredentialsById(Long userCredentialsId) {
        userCredentialsEntityRepository.deleteById(userCredentialsId);
    }

    @Override
    public UserCredentialsFull createUserCredentials(UserCredentials userCredentials) {
        var full = new UserCredentialsFull();
        full.setLogin(userCredentials.getLogin());
        full.setPassword(userCredentials.getPassword());
        var entity = entityFromCredentials(full);
        var result = userCredentialsEntityRepository.save(entity);
        return credentialsFromEntity(result);
    }

    @Override
    public UserCredentialsFull updateUserCredentials(UserCredentialsFull userCredentials) {
        var entity = entityFromCredentials(userCredentials);
        var result = userCredentialsEntityRepository.save(entity);
        return credentialsFromEntity(result);
    }

    @Override
    public Boolean isUserWithLoginExists(UserCredentials userCredentials) {
        return getUserCredentialsFullByCredentials(userCredentials) != null;
    }

    @Override
    public UserCredentialsFull getUserCredentialsFullByCredentials(UserCredentials userCredentials) {
        var entity = userCredentialsEntityRepository.findByLoginAndPasswordHash(userCredentials.getLogin(), userCredentials.getPassword().hashCode());
        return entity.map(this::credentialsFromEntity).orElse(null);
    }

    @Override
    public UserCredentialsFull getUserCredentialsFullByToken(String token) {
        var result = userCredentialsEntityRepository.findByAccessToken(token);
        return result.map(this::credentialsFromEntity).orElse(null);
    }

    @Override
    public void setAccessTokenToUser(Long userId, String token) {
        var entity = userCredentialsEntityRepository.findById(userId);
        entity.ifPresent(userCredentialsEntity -> {
            userCredentialsEntity.setAccessToken(token);
            userCredentialsEntity.setAccessTokenCreationDate(LocalDateTime.now());
        });
        userCredentialsEntityRepository.save(entity.get());
    }

    private UserCredentialsEntity entityFromCredentials(UserCredentialsFull credentials) {
        var entity = new UserCredentialsEntity();
        entity.setId(credentials.getId());
        entity.setLogin(credentials.getLogin());
        entity.setPasswordHash(credentials.getPassword().hashCode());
        entity.setAccessToken(credentials.getAccessToken());
        entity.setAccessTokenCreationDate(credentials.getAccessTokenCreationDate());
        return entity;
    }

    private UserCredentialsFull credentialsFromEntity(UserCredentialsEntity entity) {
        var credentials = new UserCredentialsFull();
        credentials.setId(entity.getId());
        credentials.setLogin(entity.getLogin());
        credentials.setAccessToken(entity.getAccessToken());
        credentials.setAccessTokenCreationDate(entity.getAccessTokenCreationDate());
        return credentials;
    }
}