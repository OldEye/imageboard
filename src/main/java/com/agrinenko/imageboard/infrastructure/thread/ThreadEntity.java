package com.agrinenko.imageboard.infrastructure.thread;

import javax.persistence.*;

@Entity
@Table(name = "threads")
public class ThreadEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "board_id")
    private Long boardId;

    @Column(name = "initial_post_id")
    private Long initialPostId;

    public ThreadEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public Long getInitialPostId() {
        return initialPostId;
    }

    public void setInitialPostId(Long initialPostId) {
        this.initialPostId = initialPostId;
    }
}