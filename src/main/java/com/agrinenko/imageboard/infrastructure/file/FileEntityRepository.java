package com.agrinenko.imageboard.infrastructure.file;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface FileEntityRepository extends PagingAndSortingRepository<FileEntity, UUID> {
}