package com.agrinenko.imageboard.infrastructure.file;

import com.agrinenko.imageboard.model.inner.FileInner;
import com.agrinenko.imageboard.repository.FileInnerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public class FileInnerRepositoryImpl implements FileInnerRepository {

    private final FileEntityRepository repository;

    @Autowired
    public FileInnerRepositoryImpl(FileEntityRepository repository) {
        this.repository = repository;
    }

    @Override
    public UUID saveFile(FileInner file) {
        var entity = entityFromInner(file);
        var result = repository.save(entity);
        return result.getToken();
    }

    @Override
    public void deleteFile(UUID token) {
        repository.deleteById(token);
    }

    @Override
    public FileInner getFile(UUID token) {
        var entity = repository.findById(token);
        return entity.map(this::innerFromEntity).orElse(null);
    }

    @Override
    public String getFileName(UUID token) {
        var result = repository.findById(token);
        return result.map(FileEntity::getName).orElse(null);
    }

    private FileInner innerFromEntity(FileEntity entity) {
        var inner = new FileInner();
        inner.setName(entity.getName());
        inner.setExtension(entity.getExtension());
        inner.setToken(entity.getToken());
        inner.setFile(entity.getFileBytes());
        return inner;
    }

    private FileEntity entityFromInner(FileInner inner) {
        var entity = new FileEntity();
        entity.setName(inner.getName());
        entity.setExtension(inner.getExtension());
        entity.setToken(inner.getToken());
        entity.setFileBytes(inner.getFile());
        return entity;
    }
}
