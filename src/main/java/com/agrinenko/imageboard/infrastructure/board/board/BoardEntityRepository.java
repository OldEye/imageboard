package com.agrinenko.imageboard.infrastructure.board.board;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BoardEntityRepository extends PagingAndSortingRepository<BoardEntity, Long> {

}