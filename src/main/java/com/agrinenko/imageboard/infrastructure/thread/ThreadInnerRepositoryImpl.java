package com.agrinenko.imageboard.infrastructure.thread;

import com.agrinenko.imageboard.model.generic.Paged;
import com.agrinenko.imageboard.model.generic.PagedRequest;
import com.agrinenko.imageboard.model.inner.ThreadInner;
import com.agrinenko.imageboard.repository.ThreadInnerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public class ThreadInnerRepositoryImpl implements ThreadInnerRepository {

    private final ThreadEntityRepository repository;

    @Autowired
    public ThreadInnerRepositoryImpl(ThreadEntityRepository repository) {
        this.repository = repository;
    }

    @Override
    public Paged<ThreadInner> getThreadsPaged(PagedRequest pagedRequest) {
        var posts = new ArrayList<ThreadInner>();
        var result = repository.findAll(PageRequest.of(pagedRequest.getPageNumber(), pagedRequest.getPageSize()));
        result.forEach(entity -> posts.add(innerFromEntity(entity)));
        return new Paged<>(posts, pagedRequest.getPageNumber(), result.getTotalPages(), pagedRequest.getPageSize(), result.getTotalElements());
    }

    @Override
    public ThreadInner getThreadById(Long threadId) {
        var result = repository.findById(threadId);
        return result.map(this::innerFromEntity).orElse(null);
    }

    @Override
    public void deleteThreadById(Long threadId) {
        repository.deleteById(threadId);
    }

    @Override
    public ThreadInner createThread(ThreadInner thread) {
        var entity = entityFromInner(thread);
        entity.setId(null);
        var result = repository.save(entity);
        return innerFromEntity(result);
    }

    private ThreadInner innerFromEntity(ThreadEntity entity) {
        var inner = new ThreadInner();
        inner.setId(entity.getId());
        inner.setBoardId(entity.getBoardId());
        inner.setInitialPostId(entity.getInitialPostId());
        return inner;
    }

    private ThreadEntity entityFromInner(ThreadInner inner) {
        var entity = new ThreadEntity();
        entity.setId(inner.getId());
        entity.setBoardId(inner.getBoardId());
        entity.setInitialPostId(inner.getInitialPostId());
        return entity;
    }
}
