package com.agrinenko.imageboard.repository;

import com.agrinenko.imageboard.model.inner.BoardInner;
import com.agrinenko.imageboard.model.generic.Paged;
import com.agrinenko.imageboard.model.generic.PagedRequest;

import java.util.List;

public interface BoardInnerRepository {

    Paged<BoardInner> getBoards(PagedRequest pagedRequest);

    BoardInner getBoardById(Long boardId);

    void deleteBoardById(Long boardId);

    BoardInner createBoard(BoardInner board);

    BoardInner updateBoard(BoardInner board);
}