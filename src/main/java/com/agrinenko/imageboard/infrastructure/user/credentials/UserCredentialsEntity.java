package com.agrinenko.imageboard.infrastructure.user.credentials;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "user_credentials")
public class UserCredentialsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "login")
    private String login;

    @Column(name = "password_hash")
    private Integer passwordHash;

    @Column(name = "access_token")
    private String accessToken;

    @Column(name = "access_token_creation_date")
    private LocalDateTime accessTokenCreationDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public LocalDateTime getAccessTokenCreationDate() {
        return accessTokenCreationDate;
    }

    public void setAccessTokenCreationDate(LocalDateTime accessTokenCreationDate) {
        this.accessTokenCreationDate = accessTokenCreationDate;
    }

    public Integer getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(Integer passwordHash) {
        this.passwordHash = passwordHash;
    }
}