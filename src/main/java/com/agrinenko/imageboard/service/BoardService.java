package com.agrinenko.imageboard.service;

import com.agrinenko.imageboard.model.Board;
import com.agrinenko.imageboard.model.generic.Paged;
import com.agrinenko.imageboard.model.generic.PagedRequest;
import com.agrinenko.imageboard.model.generic.Result;
import com.agrinenko.imageboard.model.inner.BoardInner;
import com.agrinenko.imageboard.repository.BoardInnerRepository;
import com.agrinenko.imageboard.service.generic.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class BoardService extends BaseService<Board> {

    private final String apiPath = "/boards/board/";

    private final BoardInnerRepository boardInnerRepository;
    private final FileService fileService;

    @Autowired
    public BoardService(BoardInnerRepository boardInnerRepository, FileService fileService) {
        this.boardInnerRepository = boardInnerRepository;
        this.fileService = fileService;
    }

    public Result<Paged<Board>> getBoards(PagedRequest pagedRequest) {
        var requestValidationResult = validatePageRequest(pagedRequest);
        if (requestValidationResult != null) return requestValidationResult;
        var boards = new ArrayList<Board>();
        var result = boardInnerRepository.getBoards(pagedRequest);
        result.getContent().forEach(inner -> boards.add(assembleBoardFromInner(inner)));
        return new Result<>(new Paged<>(
                boards, result.getPage(), result.getTotalPages(), result.getRequestedPageSize(), result.getTotalElements()));
    }

    public Result<Board> getBoardById(Long boardId) {
        var boardResult = boardInnerRepository.getBoardById(boardId);
        if (boardResult == null) return new Result<>(Result.ResultCode.NOT_EXISTS);
        return new Result<>(assembleBoardFromInner(boardResult));
    }

    public Result deleteBoardById(Long boardId) {
        boardInnerRepository.deleteBoardById(boardId);
        return Result.ok();
    }

    public Result<Board> createBoard(Board board) {
        var validationResult = validateBoard(board);
        if (validationResult != null) return validationResult;
        var inner = innerFromBoard(board);
        inner.setId(null);
        var result = boardInnerRepository.createBoard(inner);
        return new Result<>(assembleBoardFromInner(result));
    }

    public Result<Board> updateBoard(Board board) {
        var validationResult = validateBoard(board);
        if (validationResult != null) return validationResult;
        if (board.getId() == null) return new Result<>(Result.ResultCode.WRONG_PARAM, "Id can't be empty");
        var inner = innerFromBoard(board);
        var result = boardInnerRepository.updateBoard(inner);
        return new Result<>(assembleBoardFromInner(result));
    }

    private Result<Board> validateBoard(Board board) {
        if (board == null)
            return new Result<>(Result.ResultCode.WRONG_PARAM, "Board can't be empty");
        return null;
    }

    private Board assembleBoardFromInner(BoardInner inner) {
        var board = boardFromInner(inner);
        board.setReference(getReferenceStringById(inner.getId()));
        return board;
    }

    private Board boardFromInner(BoardInner inner) {
        var board = new Board();
        board.setId(inner.getId());
        board.setName(inner.getName());
        board.setDescription(inner.getDescription());
        board.setImageReference(fileService.getReferenceById(inner.getImageReference()).getContent());
        board.setMediaId(inner.getImageReference());
        return board;
    }

    private BoardInner innerFromBoard(Board board) {
        var inner = new BoardInner();
        inner.setId(board.getId());
        inner.setName(board.getName());
        inner.setDescription(board.getDescription());
        inner.setImageReference(board.getMediaId());
        return inner;
    }

    private String getReferenceStringById(Long boardId) {
        return apiPath + boardId.toString();
    }

    @Override
    public Result<String> getReferenceById(Long boardId) {
        if (boardInnerRepository.getBoardById(boardId) != null)
            return new Result<>(getReferenceStringById(boardId));
        return new Result<>(Result.ResultCode.NOT_EXISTS);
    }

    @Override
    public Result<Long> getIdByReference(String reference) {
        var idString = reference.replace(apiPath, "");
        try {
            var id = Long.parseLong(idString);
            if (boardInnerRepository.getBoardById(id) != null)
                return new Result<>(id);
            return new Result<>(Result.ResultCode.NOT_EXISTS);
        } catch (NumberFormatException e) {
            return new Result<>(Result.ResultCode.WRONG_PARAM, "Reference string must be reference of board");
        }
    }
}