package com.agrinenko.imageboard.api.generic;

import com.agrinenko.imageboard.model.generic.Result;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public abstract class BaseRestController {

    public BaseRestController() {
        this.defaultHttpInternalServerErrorResponse = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        this.defaultNotFoundResponse = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        this.defaultHttpOkResponse = new ResponseEntity<>(HttpStatus.OK);
        this.defaultHttpBadRequestResponse = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    private final ResponseEntity defaultHttpInternalServerErrorResponse;
    private final ResponseEntity defaultNotFoundResponse;
    private final ResponseEntity defaultHttpOkResponse;
    private final ResponseEntity defaultHttpBadRequestResponse;

    protected ResponseEntity generateResponseEntity(Result result) {
        if (result == null)
            return getDefaultHttpInternalServerErrorResponse();

        switch (result.getResult()) {
            case OK:
                if (result.getContent() != null)
                    return ResponseEntity.ok(result);
                else
                    return getDefaultHttpOkResponse();
            case NOT_EXISTS:
                if (result.getMessage() != null)
                    return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
                else
                    return getDefaultNotFoundResponse();
            case WRONG_PARAM:
                if (result.getMessage() != null)
                    return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
                else
                    return getDefaultHttpBadRequestResponse();
            default:
                if (result.getMessage() != null)
                    return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
                else
                    return getDefaultHttpInternalServerErrorResponse();
        }
    }

    private ResponseEntity getDefaultHttpInternalServerErrorResponse() {
        return defaultHttpInternalServerErrorResponse;
    }

    private ResponseEntity getDefaultNotFoundResponse() {
        return defaultNotFoundResponse;
    }

    private ResponseEntity getDefaultHttpOkResponse() {
        return defaultHttpOkResponse;
    }

    private ResponseEntity getDefaultHttpBadRequestResponse() {
        return defaultHttpBadRequestResponse;
    }
}