package com.agrinenko.imageboard.infrastructure.user.user;

import com.agrinenko.imageboard.model.User;
import com.agrinenko.imageboard.model.generic.Paged;
import com.agrinenko.imageboard.model.generic.PagedRequest;
import com.agrinenko.imageboard.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private final UserEntityRepository userEntityRepository;

    @Autowired
    public UserRepositoryImpl(UserEntityRepository userEntityRepository) {
        this.userEntityRepository = userEntityRepository;
    }

    @Override
    public Paged<User> getUsersPaged(PagedRequest pagedRequest) {
        var users = new ArrayList<User>();
        var result = userEntityRepository.findAll(PageRequest.of(pagedRequest.getPageNumber(), pagedRequest.getPageSize()));
        result.forEach(entity -> users.add(userFromEntity(entity)));
        return new Paged<>(users, pagedRequest.getPageNumber(), result.getTotalPages(), pagedRequest.getPageSize(), result.getTotalElements());
    }

    @Override
    public User getUserById(Long userId) {
        var result = userEntityRepository.findById(userId);
        return result.map(this::userFromEntity).orElse(null);
    }

    @Override
    public void deleteUserById(Long userId) {
        userEntityRepository.deleteById(userId);
    }

    @Override
    public User createUser(User user) {
        var entity = entityFromUser(user);
        var result = userEntityRepository.save(entity);
        return userFromEntity(result);
    }

    @Override
    public User updateUser(User user) {
        var entity = entityFromUser(user);
        var result = userEntityRepository.save(entity);
        return userFromEntity(result);
    }

    private UserEntity entityFromUser(User user) {
        var entity = new UserEntity();
        entity.setId(user.getId());
        entity.setNickname(user.getNickname());
        entity.setAbout(user.getAbout());
        entity.setUserRole(user.getUserRole());
        return entity;
    }

    private User userFromEntity(UserEntity entity) {
        var user = new User();
        user.setId(entity.getId());
        user.setNickname(entity.getNickname());
        user.setAbout(entity.getAbout());
        user.setUserRole(entity.getUserRole());
        return user;
    }
}