package com.agrinenko.imageboard.model.inner;

import java.util.UUID;

public class BoardInner {
    private Long id;

    private String name;
    private String description;
    private UUID imageReference;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public UUID getImageReference() {
        return imageReference;
    }

    public void setImageReference(UUID imageReference) {
        this.imageReference = imageReference;
    }
}