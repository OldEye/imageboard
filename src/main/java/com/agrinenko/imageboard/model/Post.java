package com.agrinenko.imageboard.model;

import com.agrinenko.imageboard.model.generic.Referenced;

import java.time.LocalDateTime;
import java.util.UUID;

public class Post extends Referenced {
    private Long id;
    private Long threadId;
    private User postedBy;
    private LocalDateTime postedDate;
    public String postedDateString;

    private String text;
    private UUID mediaId;

    private String replyToPostRef;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getPostedBy() {
        return postedBy;
    }

    public void setPostedBy(User postedBy) {
        this.postedBy = postedBy;
    }

    public LocalDateTime getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(LocalDateTime postedDate) {
        this.postedDate = postedDate;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getReplyToPostRef() {
        return replyToPostRef;
    }

    public void setReplyToPostRef(String replyToPostRef) {
        this.replyToPostRef = replyToPostRef;
    }

    public Long getThreadId() {
        return threadId;
    }

    public void setThreadId(Long threadId) {
        this.threadId = threadId;
    }

    public UUID getMediaId() {
        return mediaId;
    }

    public void setMediaId(UUID mediaId) {
        this.mediaId = mediaId;
    }

    public String getPostedDateString() {
        return postedDateString;
    }

    public void setPostedDateString(String postedDateString) {
        this.postedDateString = postedDateString;
    }
}