package com.agrinenko.imageboard.infrastructure.board.board;

import com.agrinenko.imageboard.model.generic.Paged;
import com.agrinenko.imageboard.model.generic.PagedRequest;
import com.agrinenko.imageboard.model.inner.BoardInner;
import com.agrinenko.imageboard.repository.BoardInnerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public class BoardInnerRepositoryImpl implements BoardInnerRepository {

    private final BoardEntityRepository boardEntityRepository;

    @Autowired
    public BoardInnerRepositoryImpl(BoardEntityRepository boardEntityRepository) {
        this.boardEntityRepository = boardEntityRepository;
    }

    @Override
    public Paged<BoardInner> getBoards(PagedRequest pagedRequest) {
        var boards = new ArrayList<BoardInner>();
        var result = boardEntityRepository.findAll(PageRequest.of(pagedRequest.getPageNumber(), pagedRequest.getPageSize()));
        result.forEach(entity -> boards.add(boardFromEntity(entity)));
        return new Paged<>(boards, pagedRequest.getPageNumber(), result.getTotalPages(), pagedRequest.getPageSize(), result.getTotalElements());
    }

    @Override
    public BoardInner getBoardById(Long boardId) {
        var entity = boardEntityRepository.findById(boardId);
        return entity.map(this::boardFromEntity).orElse(null);
    }

    @Override
    public void deleteBoardById(Long boardId) {
        boardEntityRepository.deleteById(boardId);
    }

    @Override
    public BoardInner createBoard(BoardInner board) {
        var entity = entityFromBoard(board);
        var resultEntity = boardEntityRepository.save(entity);
        return boardFromEntity(resultEntity);
    }

    @Override
    public BoardInner updateBoard(BoardInner board) {
        var entity = entityFromBoard(board);
        var resultEntity = boardEntityRepository.save(entity);
        return boardFromEntity(resultEntity);
    }

    private BoardInner boardFromEntity(BoardEntity entity) {
        var board = new BoardInner();
        board.setId(entity.getId());
        board.setName(entity.getName());
        board.setDescription(entity.getDescription());
        board.setImageReference(entity.getImageId());

        return board;
    }

    private BoardEntity entityFromBoard(BoardInner board) {
        BoardEntity entity = new BoardEntity();
        entity.setId(board.getId());
        entity.setName(board.getName());
        entity.setDescription(board.getDescription());
        entity.setImageId(board.getImageReference());
        return entity;
    }
}