package com.agrinenko.imageboard.repository;

import com.agrinenko.imageboard.model.inner.PostInner;

import java.util.List;

public interface PostInnerRepository {

    PostInner getPostById(Long postId);

    void deletePostById(Long postId);

    PostInner createPost(PostInner post);

    List<PostInner> getPostsByThreadId(Long threadId);
}