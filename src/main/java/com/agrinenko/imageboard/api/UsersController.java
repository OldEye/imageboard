package com.agrinenko.imageboard.api;

import com.agrinenko.imageboard.api.generic.BaseRestController;
import com.agrinenko.imageboard.model.User;
import com.agrinenko.imageboard.model.UserCredentials;
import com.agrinenko.imageboard.model.UserRole;
import com.agrinenko.imageboard.model.generic.PagedRequest;
import com.agrinenko.imageboard.model.generic.Result;
import com.agrinenko.imageboard.service.RecaptchaService;
import com.agrinenko.imageboard.service.UserService;
import org.springframework.http.HttpCookie;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@CrossOrigin
@RequestMapping("api/users")
public class UsersController extends BaseRestController {

    private final UserService service;

    private final RecaptchaService recaptchaService;

    public UsersController(UserService service, RecaptchaService recaptchaService) {
        this.service = service;
        this.recaptchaService = recaptchaService;
    }

    @GetMapping("/paged")
    public ResponseEntity getUsersPaged(@RequestBody PagedRequest pagedRequest) {
        Result result = service.getUsersPaged(pagedRequest);
        return generateResponseEntity(result);
    }

    @GetMapping("/{userId}")
    public ResponseEntity getUserById(@PathVariable Long userId) {
        Result result = service.getUserById(userId);
        return generateResponseEntity(result);
    }

    @DeleteMapping("/{userId}")
    public ResponseEntity deleteUserById(@RequestHeader(name = "Authorization") String token, @PathVariable Long userId) {
        var tokenResult = service.getUserByToken(token);
        if (tokenResult.isOk()) {
            var currentUser = tokenResult.getContent();
            if (currentUser.getUserRole() != UserRole.ADMINISTRATOR) {
                Result result = service.deleteUserById(userId);
                return generateResponseEntity(result);
            }
        }
        return generateResponseEntity(new Result(Result.ResultCode.FORBIDDEN));
    }

    @PutMapping
    public ResponseEntity updateUser(@RequestHeader(name = "Authorization") String token, @RequestBody User user) {
        var tokenResult = service.getUserByToken(token);
        if (tokenResult.isOk()) {
            var currentUser = tokenResult.getContent();
            if (currentUser.getId().equals(user.getId()) && user.getUserRole() == currentUser.getUserRole()) {
                Result result = service.updateUser(user);
                return generateResponseEntity(result);
            }
        }
        return generateResponseEntity(new Result(Result.ResultCode.FORBIDDEN));
    }

    @CrossOrigin
    @PostMapping("/registration")
    public ResponseEntity createUserCredentials(@RequestBody UserCredentials userCredentials,
//                                                @RequestParam(name = "captchaResponse") String recaptchaResponse,
                                                HttpServletRequest request) {
//        String ip = request.getRemoteAddr();
//        String captchaVerifyMessage = recaptchaService.verifyRecaptcha(ip, recaptchaResponse);
//        if(captchaVerifyMessage != null)
//            return generateResponseEntity(new Result(Result.ResultCode.FORBIDDEN, "Captcha hasn't solved yet!"));

        Result<String> result = service.createUser(userCredentials);

        if (result.isOk()) {
            HttpCookie cookie = ResponseCookie.from("accessToken", result.getContent()).build();
            return ResponseEntity.ok()
                    .header(HttpHeaders.SET_COOKIE, cookie.toString())
                    .body(result);
        } else return generateResponseEntity(result);
    }

    @PutMapping("/credentials")
    public ResponseEntity updateUserCredentials(@RequestHeader(name = "Authorization") String token, @RequestBody UserCredentials userCredentials) {
        var tokenResult = service.getUserByToken(token);
        if (tokenResult.isOk()) {
            Result result = service.updateUserCredentials(tokenResult.getContent().getId(), userCredentials);
            return generateResponseEntity(result);
        }
        return generateResponseEntity(new Result(Result.ResultCode.FORBIDDEN));
    }

    @CrossOrigin
    @PostMapping("/login")
    public ResponseEntity getTokenByCredentials(@RequestBody UserCredentials userCredentials) {
        Result<String> result = service.getTokenByCredentials(userCredentials);

        if (result.isOk()) {
            HttpCookie cookie = ResponseCookie.from("accessToken", result.getContent()).build();
            return ResponseEntity.ok()
                    .header(HttpHeaders.SET_COOKIE, cookie.toString())
                    .body(result);
        } else return generateResponseEntity(result);
    }

    @GetMapping("/current")
    public String getUsernameByToken(@CookieValue("accessToken") String accessToken) {
        var user = service.getUserByToken(accessToken);
        if (user.isOk()) return user.getContent().getNickname();
        else return "Anonymous";
    }

    @GetMapping("/current/isAdmin")
    public boolean getUserRoleByToken(@CookieValue("accessToken") String accessToken) {
        var user = service.getUserByToken(accessToken);
        if (user.isOk()) return user.getContent().getUserRole() == UserRole.ADMINISTRATOR;
        return false;
    }

    @PostMapping("/logout")
    public void logout(@CookieValue("accessToken") String accessToken) {
        var user = service.getUserByToken(accessToken);
        if (user.isOk())
            service.generateAccessToken(user.getContent().getId());
    }
}