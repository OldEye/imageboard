package com.agrinenko.imageboard.model.generic;

import javax.validation.constraints.NotNull;

public class Result<T> {

    private static Result okResult = new Result<>(ResultCode.OK);
    private static Result notExistsResult = new Result<>(ResultCode.NOT_EXISTS);

    private T content = null;
    private ResultCode result;
    private String message;

    public Result(@NotNull ResultCode result) {
        this.result = result;
    }

    public Result(T content) {
        this.content = content;
        result = ResultCode.OK;
    }

    public Result(T content, @NotNull ResultCode result) {
        this.content = content;
        this.result = result;
    }

    public Result(@NotNull ResultCode result, String message) {
        this.result = result;
        this.message = message;
    }

    public Result(T content, @NotNull ResultCode result, String message) {
        this.content = content;
        this.result = result;
        this.message = message;
    }

    public T getContent() {
        return content;
    }

    public void setContent(T content) {
        this.content = content;
    }

    public ResultCode getResult() {
        return result;
    }

    public boolean isOk() {
        return result == ResultCode.OK;
    }

    public void setResult(@NotNull ResultCode result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public enum ResultCode {
        OK,
        NOT_EXISTS,
        WRONG_PARAM,
        FORBIDDEN
    }

    public static Result ok() {
        return okResult;
    }

    public static Result notExists() {
        return notExistsResult;
    }
}