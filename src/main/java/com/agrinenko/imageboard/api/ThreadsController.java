package com.agrinenko.imageboard.api;

import com.agrinenko.imageboard.api.generic.BaseRestController;
import com.agrinenko.imageboard.model.Post;
import com.agrinenko.imageboard.model.Thread;
import com.agrinenko.imageboard.model.ThreadLoadingPolicy;
import com.agrinenko.imageboard.model.User;
import com.agrinenko.imageboard.model.generic.PagedRequest;
import com.agrinenko.imageboard.model.generic.Result;
import com.agrinenko.imageboard.service.FileService;
import com.agrinenko.imageboard.service.ThreadService;
import com.agrinenko.imageboard.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.UUID;

@RestController
@RequestMapping("api/threads")
public class ThreadsController extends BaseRestController {

    private final ThreadService service;
    private final UserService userService;
    private final FileService fileService;

    @Autowired
    public ThreadsController(ThreadService service, UserService userService, FileService fileService) {
        this.service = service;
        this.userService = userService;
        this.fileService = fileService;
    }

    @GetMapping("paged")
    public ResponseEntity getThreadsPaged(@RequestBody PagedRequest pagedRequest, @RequestParam String loadingPolicy) {
        ThreadLoadingPolicy policy = ThreadLoadingPolicy.byReduction(loadingPolicy);
        Result result = service.getThreadsPaged(pagedRequest, policy);
        return generateResponseEntity(result);
    }

    @GetMapping("{threadId}")
    public ResponseEntity getThreadById(@PathVariable Long threadId, @RequestParam String loadingPolicy) {
        ThreadLoadingPolicy policy = ThreadLoadingPolicy.byReduction(loadingPolicy);
        Result result = service.getThreadById(threadId, policy);
        return generateResponseEntity(result);
    }

    @DeleteMapping("/{threadId}")
    public ResponseEntity deleteThreadById(@PathVariable Long threadId) {
        Result result = service.deleteThreadById(threadId);
        return generateResponseEntity(result);
    }

    @PostMapping
    public ResponseEntity createThread(@CookieValue("accessToken") String accessToken,
                                       @RequestParam String text, @RequestParam Long boardId,
                                       @RequestParam(value = "file", required = false) UUID fileId) throws IOException {
        var user = new User();
        if (accessToken != null && !accessToken.isEmpty()) {
            var userRes = userService.getUserByToken(accessToken);
            if (userRes.isOk()) user = userRes.getContent();
        } else return null;

        var thread = new Thread();
        var initialPost = new Post();
        initialPost.setPostedBy(user);
        initialPost.setText(text);
        if (fileId != null)
            initialPost.setMediaId(fileId);
        thread.setInitialPost(initialPost);
        thread.setBoardId(boardId);
        Result result = service.createThread(thread);
        return generateResponseEntity(result);
    }

    @PostMapping("/posts")
    public ResponseEntity createPost(@CookieValue("accessToken") String accessToken,
                                     @RequestParam String text, @RequestParam Long threadId
    ) {
        var user = new User();
        user.setNickname("Anonymous");
        if (accessToken != null && !accessToken.isEmpty()) {
            var userRes = userService.getUserByToken(accessToken);
            if (userRes.isOk()) user = userRes.getContent();
        } else return null;
        var post = new Post();
        post.setText(text);
        post.setPostedBy(user);
        post.setThreadId(threadId);
        Result result = service.createPost(post);
        return generateResponseEntity(result);
    }
}