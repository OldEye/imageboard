package com.agrinenko.imageboard.service;

import com.agrinenko.imageboard.model.generic.Result;
import com.agrinenko.imageboard.model.inner.FileInner;
import com.agrinenko.imageboard.repository.FileInnerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class FileService {

    private final String apiPath = "/api/files/";

    private final FileInnerRepository fileInnerRepository;

    @Autowired
    public FileService(FileInnerRepository fileInnerRepository) {
        this.fileInnerRepository = fileInnerRepository;
    }

    public Result<UUID> createFile(FileInner file) {
        var validationResult = validateFileInner(file);
        if (validationResult != null) return validationResult;
        var result = fileInnerRepository.saveFile(file);
        return new Result<>(result);
    }

    public Result deleteFile(UUID token) {
        fileInnerRepository.deleteFile(token);
        return Result.ok();
    }

    public Result<FileInner> getFile(UUID token) {
        if (token != null) {
            var result = fileInnerRepository.getFile(token);
            if (result != null) return new Result<>(result);
        }
        return new Result<>(Result.ResultCode.NOT_EXISTS);
    }

    public Result<String> getFileName(UUID token) {
        if (token != null) {
            var result = fileInnerRepository.getFileName(token);
            if (result != null) return new Result<>(result);
        }
        return new Result<>(Result.ResultCode.NOT_EXISTS);
    }

    private Result<UUID> validateFileInner(FileInner fileInner) {
        if (fileInner.getExtension() == null || fileInner.getExtension().isEmpty())
            return new Result<>(Result.ResultCode.WRONG_PARAM, "File must have valid extension");
        if (fileInner.getFile() == null || fileInner.getFile().length == 0)
            return new Result<>(Result.ResultCode.WRONG_PARAM, "File must have any content");
        if (fileInner.getName() == null || fileInner.getName().isEmpty())
            return new Result<>(Result.ResultCode.WRONG_PARAM, "File must have valid name");
        return null;
    }

    public Result<String> getReferenceById(UUID resourceId) {
        if (resourceId == null) return new Result<>(null);
        return new Result<>(apiPath + resourceId.toString());
    }

    public Result<UUID> getIdByReference(String reference) {
        var string = reference.replace(apiPath, "");
        try {
            var uuid = UUID.fromString(string);
            if (getFile(uuid).isOk())
                return new Result<>(uuid);
            return null;
        } catch (Exception e) {
            return null;
        }
    }
}