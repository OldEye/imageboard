package com.agrinenko.imageboard.infrastructure.post;

import com.agrinenko.imageboard.model.inner.PostInner;
import com.agrinenko.imageboard.repository.PostInnerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class PostInnerRepositoryImpl implements PostInnerRepository {

    private final PostEntityRepository repository;

    @Autowired
    public PostInnerRepositoryImpl(PostEntityRepository repository) {
        this.repository = repository;
    }

    @Override
    public PostInner getPostById(Long postId) {
        var result = repository.findById(postId);
        return result.map(this::innerFromEntity).orElse(null);
    }

    @Override
    public void deletePostById(Long postId) {
        repository.deleteById(postId);
    }

    @Override
    public PostInner createPost(PostInner post) {
        var entity = entityFromInner(post);
        entity.setId(null);
        var result = repository.save(entity);
        return innerFromEntity(result);
    }

    @Override
    public List<PostInner> getPostsByThreadId(Long threadId) {
        var list = new ArrayList<PostInner>();
        var entities = repository.findAllByThreadId(threadId);
        entities.forEach(entity -> list.add(innerFromEntity(entity)));
        return list;
    }

    private PostInner innerFromEntity(PostEntity entity) {
        var inner = new PostInner();
        inner.setId(entity.getId());
        inner.setText(entity.getText());
        inner.setPostedByUserId(entity.getPostedByUserId());
        inner.setPostedDate(entity.getPostedDate());
        inner.setThreadId(entity.getThreadId());
        inner.setMediaId(entity.getMediaId());
        return inner;
    }

    private PostEntity entityFromInner(PostInner inner) {
        var entity = new PostEntity();
        entity.setId(inner.getId());
        entity.setThreadId(inner.getThreadId());
        entity.setText(inner.getText());
        entity.setPostedByUserId(inner.getPostedByUserId());
        entity.setPostedDate(inner.getPostedDate());
        entity.setMediaId(inner.getMediaId());
        return entity;
    }
}