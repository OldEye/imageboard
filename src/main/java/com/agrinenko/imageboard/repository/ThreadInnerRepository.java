package com.agrinenko.imageboard.repository;

import com.agrinenko.imageboard.model.generic.Paged;
import com.agrinenko.imageboard.model.generic.PagedRequest;
import com.agrinenko.imageboard.model.inner.ThreadInner;

public interface ThreadInnerRepository {

    Paged<ThreadInner> getThreadsPaged(PagedRequest pagedRequest);

    ThreadInner getThreadById(Long threadId);

    void deleteThreadById(Long threadId);

    ThreadInner createThread(ThreadInner thread);

}