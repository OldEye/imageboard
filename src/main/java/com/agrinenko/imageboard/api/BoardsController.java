package com.agrinenko.imageboard.api;

import com.agrinenko.imageboard.api.generic.BaseRestController;
import com.agrinenko.imageboard.model.Board;
import com.agrinenko.imageboard.model.UserRole;
import com.agrinenko.imageboard.model.generic.PagedRequest;
import com.agrinenko.imageboard.model.generic.Result;
import com.agrinenko.imageboard.service.BoardService;
import com.agrinenko.imageboard.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("api/boards")
public class BoardsController extends BaseRestController {

    private final BoardService service;
    private final UserService userService;

    public BoardsController(BoardService service, UserService userService) {
        this.service = service;
        this.userService = userService;
    }

//    @GetMapping
//    public ResponseEntity getBoardsPaged(@RequestParam(name = "pageNumber") Integer pageNumber, @RequestParam(name = "pageSize") Integer pageSize) {
//        var pageRequest = new PagedRequest();
//        pageRequest.setPageNumber(pageNumber);
//        pageRequest.setPageSize(pageSize);
//        Result result = service.getBoards(pageRequest);
//        return generateResponseEntity(result);
//    }

    @GetMapping
    public ResponseEntity getBoards() {
        var pageRequest = new PagedRequest();
        pageRequest.setPageNumber(0);
        pageRequest.setPageSize(100);
        Result result = service.getBoards(pageRequest);
        return generateResponseEntity(result);
    }

    @GetMapping("{boardId}")
    public ResponseEntity getBoardById(@PathVariable Long boardId) {
        Result result = service.getBoardById(boardId);
        return generateResponseEntity(result);
    }

    @DeleteMapping("{boardId}")
    public ResponseEntity deleteBoardById(@RequestHeader(name = "Authorization") String token, @PathVariable Long boardId) {
        var tokenResult = userService.getUserByToken(token);
        if (tokenResult.isOk()) {
            var currentUser = tokenResult.getContent();
            if (currentUser.getUserRole() != UserRole.ADMINISTRATOR) {
                Result result = service.deleteBoardById(boardId);
                return generateResponseEntity(result);
            }
        }
        return generateResponseEntity(new Result(Result.ResultCode.FORBIDDEN));
    }

    @PostMapping
    public ResponseEntity createBoard(@CookieValue("accessToken") String accessToken,
                                      @RequestParam String name,
                                      @RequestParam String description,
                                      @RequestParam(value = "file", required = false) UUID fileId) {
        if (accessToken != null && !accessToken.isEmpty()) {
            var userRes = userService.getUserByToken(accessToken);
            if (userRes.isOk() && userRes.getContent().getUserRole() == UserRole.ADMINISTRATOR) {
                var board = new Board();
                board.setName(name);
                board.setDescription(description);
                board.setMediaId(fileId);
                var result = service.createBoard(board);
                return generateResponseEntity(result);
            } else return null;
        } else return null;
    }

    @PutMapping
    public ResponseEntity updateBoard(@RequestHeader(name = "Authorization") String token, @RequestBody Board board) {
        var tokenResult = userService.getUserByToken(token);
        if (tokenResult.isOk()) {
            var currentUser = tokenResult.getContent();
            if (currentUser.getUserRole() != UserRole.ADMINISTRATOR) {
                Result result = service.updateBoard(board);
                return generateResponseEntity(result);
            }
        }
        return generateResponseEntity(new Result(Result.ResultCode.FORBIDDEN));
    }
}