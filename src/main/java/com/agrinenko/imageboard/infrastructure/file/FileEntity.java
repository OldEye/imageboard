package com.agrinenko.imageboard.infrastructure.file;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "files")
public class FileEntity {

    @Id
    @Column(name = "token")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID token;

    @Column(name = "file_bytes")
    private byte[] fileBytes;

    @Column(name = "name")
    private String name;

    @Column(name = "extension")
    private String extension;

    public FileEntity() {
    }

    public byte[] getFileBytes() {
        return fileBytes;
    }

    public void setFileBytes(byte[] fileBytes) {
        this.fileBytes = fileBytes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public UUID getToken() {
        return token;
    }

    public void setToken(UUID token) {
        this.token = token;
    }
}